import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyCrTA9LK5uIuqpvnKgw0Iwh_F-4rNalppw",
  authDomain: "orbit-db-ec8d5.firebaseapp.com",
  databaseURL: "https://orbit-db-ec8d5.firebaseio.com",
  projectId: "orbit-db-ec8d5",
  storageBucket: "orbit-db-ec8d5.appspot.com",
  messagingSenderId: "17929616095",
  appId: "1:17929616095:web:09a172efb908887345076a",
  measurementId: "G-C0GMFM3MYW",
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();
const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });
export const signInWithGoogle = () => auth.signInWithPopup(provider);
export default firebase;
